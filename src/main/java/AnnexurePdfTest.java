import com.lowagie.text.DocumentException;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import com.openhtmltopdf.svgsupport.BatikSVGDrawer;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;

public class AnnexurePdfTest {


    public static void main(String[] args) throws Exception {

        System.out.println(new BigDecimal("0").add(BigDecimal.ONE));
        System.out.println(new BigDecimal("0.0").add(BigDecimal.ONE));


//        String invoiceHtml = FileUtils.readFileToString(new File("src/main/resources/annexurePdf.html"), "ISO-8859-1");
//        byte[] invoicePdf = generatePdf(invoiceHtml);
//        String path = "src/main/annexurePdf.pdf";
//        FileOutputStream stream = new FileOutputStream(path);
//        try {
//            stream.write(invoicePdf);
//        } finally {
//            stream.close();
//        }
    }

    public static byte[] generatePdf(String html) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PdfRendererBuilder builder = new PdfRendererBuilder().useSVGDrawer(new BatikSVGDrawer()).withHtmlContent(html, null).toStream(os);

        builder.run();
        try {
            os.close();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        return os.toByteArray();
    }

    public static byte[] generatePdfFlyingSaucer(String html) throws DocumentException {
        ITextRenderer renderer = new ITextRenderer();

// if you have html source in hand, use it to generate document object
        renderer.setDocumentFromString(html);
        renderer.layout();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        renderer.createPDF(os);
        try {
            os.close();
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        return os.toByteArray();
    }

}
