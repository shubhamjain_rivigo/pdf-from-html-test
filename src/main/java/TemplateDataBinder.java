import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

public class TemplateDataBinder {

    private VelocityEngine velocityEngine = new VelocityEngine();

    public TemplateDataBinder(){
        initializeVelocityEngine(velocityEngine);
    }

    public String bindTemplateData(String templateName, Map<String, Object> data) {
        Template template = velocityEngine.getTemplate(templateName);
        VelocityContext context = new VelocityContext();
        for (String key : data.keySet()) {
            context.put(key, data.get(key));
        }
        context.put("DateTimeFormat", DateTimeFormat.class);
        context.put("DateTimeZone", DateTimeZone.class);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

    private void initializeVelocityEngine(VelocityEngine velocityEngine) {
        Properties velocityProperties = new Properties();
        velocityProperties.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityProperties.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init(velocityProperties);
    }
}