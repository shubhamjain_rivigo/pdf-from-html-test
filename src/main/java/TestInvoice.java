import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import com.openhtmltopdf.svgsupport.BatikSVGDrawer;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TestInvoice {

    private static final String ITCINVOICE_VM = "ITCInvoice.vm";
    private static final String NESTLE_INVOICE_VM = "NestleInvoice.vm";

    public static void main(String[] args) throws Exception {
        TemplateDataBinder templateDataBinder = new TemplateDataBinder();
        Map<String, Object> paramMap = new HashMap();
        paramMap.put("invoiceHeader", "TAX INVOICE");
		paramMap.put("clientName", "Amazon");
		paramMap.put("description", "Being Transportation Charges as per annexure attached");
		paramMap.put("clientAddress", StringEscapeUtils.escapeHtml("Amazon & web services").replace("&nbsp;", " "));
		paramMap.put("clientGST", "GSTNUMBER");
		paramMap.put("clientPAN", "ABCD12345A");
		paramMap.put("invoiceNumber", "RZIABC12312312");
		paramMap.put("invoiceDate", DateTime.now().withZone(DateTimeZone.forOffsetHoursMinutes(5,30)));
		paramMap.put("baseAmount", new BigDecimal(1000.30));
        paramMap.put("discountAmount", null);
        paramMap.put("taxableAmount", new BigDecimal(900));
		paramMap.put("invoiceAmount", new BigDecimal(2000.30));
		paramMap.put("invoiceAmountString", "One thousand rupees and thirty paise");
		paramMap.put("invoiceDescription", StringEscapeUtils.escapeHtml("Test invoice"));
		paramMap.put("rivigoAddress", StringEscapeUtils.escapeHtml("Rivigo Corporate Office, Gurgaon"));
		paramMap.put("rivigoGST", StringEscapeUtils.escapeHtml("GSTNUMBER"));
		paramMap.put("gstFirstComponent", "First Component");
		paramMap.put("gstSecondComponent", "Second Component");
		paramMap.put("gstFirstComponentPercentage", "10%");
		paramMap.put("gstSecondComponentPercentage", "10%");
		paramMap.put("gstFirstComponentHeader", "First component Header");
		paramMap.put("gstSecondComponentHeader", "Second Component Header");
		paramMap.put("stateName", "Haryana");
		paramMap.put("stateGstCode", "HR");
		paramMap.put("rivigoHSN", "fdsaf");
		paramMap.put("igstPercent", "12");
		paramMap.put("igstAmount", "100");
        paramMap.put("rivigoPAN","PAN");
        paramMap.put("cgstAmount", "100");
        paramMap.put("sgstAmount", "100");
    	String invoiceHtml = templateDataBinder.bindTemplateData(NESTLE_INVOICE_VM, paramMap);
        byte[] invoicePdf = generatePdf(invoiceHtml);
        String path = "src/main/mytest.pdf";
		FileOutputStream stream = new FileOutputStream(path);
		try {
			stream.write(invoicePdf);
		} finally {
			stream.close();
		}
    }

	public static byte[] generatePdf(String html) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		PdfRendererBuilder builder = new PdfRendererBuilder()
				.useSVGDrawer(new BatikSVGDrawer())
				.withHtmlContent(html, null)
				.toStream(os);

		builder.run();
		try {
			os.close();
		} catch (IOException ioe) {
			System.out.println(ioe);
		}
		return os.toByteArray();
	}
}
