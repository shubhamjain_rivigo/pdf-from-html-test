/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AddImageToPDFOpenPdfLibrary {

    public static void main(String[] args) {

        System.out.println("Absolute Positioning of an Image");

        // step 1: creation of a document-object
        Document document = new Document();

        try {

            // step 2:
            // we create a writer that listens to the document
            // and directs a PDF-stream to a file

//            PdfWriter.getInstance(document, new FileOutputStream("src/main/pdfWithImage1.pdf"));
//
//            // step 3: we open the document
//            document.open();

            String in = "src/main/pdfWithImage1.pdf";
            String out = "src/main/pdfWithImage1.pdf";

            PdfReader reader = new PdfReader(in);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfStamper stamper = new PdfStamper(reader, baos );

            int totalPages = reader.getNumberOfPages();
            stamper.insertPage(totalPages + 1, PageSize.A4);
            stamper.addAnnotation(PdfAnnotation.createText(stamper.getWriter(), new Rectangle(30f, 750f, 80f, 800f), "inserted page", "This page is the title page.", true, null),
                    reader.getNumberOfPages());
            stamper.close();

            FileOutputStream fileOutputStream = new FileOutputStream(out);
            IOUtils.write(baos.toByteArray(), fileOutputStream);



            // step 4: we add content
//            Image png = Image.getInstance("src/main/image.jpg");
//            png.setAbsolutePosition(0, 0);
//            document.add(png);
        }
        catch(DocumentException de) {
            System.err.println(de.getMessage());
        }
        catch(IOException ioe) {
            System.err.println(ioe.getMessage());
        }

        // step 5: we close the document
        document.close();
    }

    public void createPDFFromImage(String imagePath, String outputFile)
            throws IOException {
        PDDocument document = new PDDocument();
        PDPage page = new PDPage(PDRectangle.A4);
        PDRectangle mediaBox = page.getMediaBox();
        document.addPage(page);
        BufferedImage image = ImageIO.read(new File(imagePath));
        PDImageXObject pdImage = LosslessFactory.createFromImage(document, image);

        try (PDPageContentStream contentStream = new PDPageContentStream(document, page, AppendMode.APPEND, true, true)) {
            addCenteredTextToStream(contentStream, "Supporting Documents for Invoice Number CZIDL18190009626 created on 23-11-2018", PDType1Font.HELVETICA_BOLD, 14, 20, mediaBox.getWidth(), mediaBox.getHeight());
            addCenteredTextToStream(contentStream, "Trip Number:7XXXXXX (4th Oct 2018 – 8th Oct 2018) – STN", PDType1Font.HELVETICA_BOLD, 12, 50, mediaBox.getWidth(), mediaBox.getHeight());
            addCenteredImageToStream(contentStream, pdImage, 80, 20, mediaBox.getWidth(), mediaBox.getHeight());
            addRivigoLogo(document, contentStream, mediaBox.getWidth(), mediaBox.getHeight());
        }
        catch (Exception e) {
        }
        document.save(outputFile);
        document.close();
    }

    public static void addCenteredTextToStream(PDPageContentStream contentStream, String text, PDFont font, int fontSize, int marginTop, float pageWidth, float pageHeight) throws IOException {
        float titleWidth = font.getStringWidth(text) / 1000 * fontSize;
        float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
        float startX = (pageWidth - titleWidth) / 2;
        float startY = pageHeight - marginTop - titleHeight;
        contentStream.beginText();
        contentStream.setFont(font, fontSize);
        contentStream.newLineAtOffset(startX, startY);
        contentStream.showText(text);
        contentStream.endText();
    }

    public static void addCenteredImageToStream(PDPageContentStream contentStream, PDImageXObject pdImage, int marginTop, int minSideMargin, float pageWidth, float pageHeight) throws IOException {
        float maxImageWidth = pageWidth - 2 * minSideMargin;
        float maxImageHeight = pageHeight - marginTop - minSideMargin;
        float scale = Math.min(maxImageWidth / pdImage.getWidth(), maxImageHeight / pdImage.getHeight());
        contentStream.drawImage(pdImage, (pageWidth - pdImage.getWidth() * scale) / 2, pageHeight - marginTop - pdImage.getHeight() * scale, pdImage.getWidth() * scale, pdImage.getHeight() * scale);
    }

    public static void addRivigoLogo(PDDocument document, PDPageContentStream contentStream, float pageWidth, float pageHeight) throws IOException {
        BufferedImage rivigoLogo = ImageIO.read(new File("src/main/Rivigo_Logo.png"));
        PDImageXObject rivigologoImage = LosslessFactory.createFromImage(document, rivigoLogo);
        contentStream.drawImage(rivigologoImage, pageWidth - 60, 4, 50, 11);
    }

}
